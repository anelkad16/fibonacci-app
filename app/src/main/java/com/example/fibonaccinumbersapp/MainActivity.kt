package com.example.fibonaccinumbersapp

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.example.fibonaccinumbersapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private val viewModel by viewModels<MainViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        binding.apply {
            startBtn.setOnClickListener { startCounting() }
            cancelBtn.setOnClickListener { cancelCounting() }
        }
        setContentView(binding.root)

        viewModel.state.observe(this) { state ->
            when (state) {
                is MainActivityState.NumberChanged -> binding.textView.text = state.message
                MainActivityState.Initial -> Unit
                MainActivityState.StoppedProcess -> cancelCounting()
                MainActivityState.UiBlocked -> blockUi()
                MainActivityState.UiUnblock -> unblockUi()
            }
        }
    }

    private fun blockUi() = with(binding) {
        editTextView.isEnabled = false
        startBtn.isVisible = false
        cancelBtn.isVisible = true
    }

    private fun unblockUi() = with(binding) {
        editTextView.isEnabled = true
        startBtn.isVisible = true
        cancelBtn.isVisible = false
    }

    private fun startCounting() {
        val inputValue = binding.editTextView.text.toString().toInt()
        viewModel.startCalculation(inputValue)
    }

    private fun cancelCounting() {
        viewModel.cancelCalculation()
        binding.textView.text = "Calculation is canceled"
    }

}

