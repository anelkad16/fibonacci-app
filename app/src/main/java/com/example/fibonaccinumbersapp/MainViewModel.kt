package com.example.fibonaccinumbersapp

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {

    private val _state: MutableLiveData<MainActivityState> =
        MutableLiveData(MainActivityState.Initial)
    val state: LiveData<MainActivityState>
        get() = _state

    private var currentValue = 0
    private lateinit var job: Job

    private fun getNextNumber(step: Int): Int {
        if (step == 0) return 0
        if (step == 1) return 1
        return getNextNumber(step - 1) + getNextNumber(step - 2)
    }

    fun startCalculation(count: Int) {
        job = viewModelScope.launch {
            _state.value = MainActivityState.UiBlocked
            for (i in 1..count) {
                currentValue = getNextNumber(i)
                val text = "Calculation is running: $currentValue"
                _state.value = MainActivityState.NumberChanged(text)
                delay(1000)
            }
            val text = "Result is: $currentValue"
            _state.value = MainActivityState.NumberChanged(text)
            _state.value = MainActivityState.UiUnblock
        }
    }

    fun cancelCalculation() {
        job.cancel()
        _state.value = MainActivityState.StoppedProcess
        _state.value = MainActivityState.UiUnblock
    }
}

sealed interface MainActivityState {
    object Initial : MainActivityState
    object StoppedProcess : MainActivityState
    object UiBlocked: MainActivityState
    object UiUnblock: MainActivityState
    data class NumberChanged(val message: String) : MainActivityState
}